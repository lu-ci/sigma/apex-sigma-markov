# BUILDER

FROM rust:1.52-slim as builder

RUN apt-get update
RUN apt-get install -y libssl-dev pkg-config

WORKDIR /srv/asm
COPY . .

RUN cargo build --release --workspace

# APPLICATION

FROM rust:1.52-slim

ARG user_uid=1000
ARG user_gid=1000

RUN addgroup --system --gid="$user_gid" app
RUN adduser --system --ingroup="app" --uid="$user_uid" app

RUN mkdir -p /app/modules && chown -R app:app /app

WORKDIR /app
USER app

COPY --chown=app:app --from=builder /srv/asm/target/release/apex-sigma-markov /app/asm

ENTRYPOINT ["./asm"]
