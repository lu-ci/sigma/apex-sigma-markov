use std::collections::HashMap;

use warp::Filter;

use crate::config::ASMConfig;
use crate::controllers::combined::combined_handler;
use crate::controllers::single::single_handler;

pub struct ASMCore {
    pub cfg: ASMConfig,
}

impl ASMCore {
    pub fn new() -> anyhow::Result<Self> {
        let cfg = ASMConfig::new()?;
        Ok(Self { cfg })
    }

    pub async fn run(&self) -> anyhow::Result<()> {
        let srv = Self::new()?;
        println!("Starting the backend on {} ...", srv.cfg.get_uri());
        let host = srv.cfg.get_host()?;
        let oauth = warp::any().and(
            warp::path("single")
                .and(warp::filters::path::param::<u64>())
                .and(warp::filters::query::query::<HashMap<String, String>>())
                .and_then(single_handler)
                .or(warp::path("combined")
                    .and(warp::filters::path::param::<String>())
                    .and(warp::filters::query::query::<HashMap<String, String>>())
                    .and_then(combined_handler)
                )
        );
        warp::serve(oauth).bind(host).await;
        Ok(())
    }
}
