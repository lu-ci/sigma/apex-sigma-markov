use markov::Chain;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct ASMChain {
    pub state_size: u64,
    pub chain: String,
    pub parsed_sentences: Vec<Vec<String>>
}

impl ASMChain {
    pub fn load(content: String) -> anyhow::Result<Self> {
        Ok(serde_json::from_str::<Self>(&content)?)
    }

    pub fn get_stats(&self) -> [u64; 2] {
        let sentences = self.parsed_sentences.len() as u64;
        let mut cells = 0_u64;
        for ps in &self.parsed_sentences {
            cells += ps.len() as u64;
        }
        [sentences, cells]
    }

    pub fn get_sentences(&self) -> Vec<String> {
        let mut sentences = Vec::new();
        for ps in &self.parsed_sentences {
            sentences.push(ps.join(" "));
        }
        sentences
    }

    pub fn get_chain(&self) -> Chain<String> {
        let mut chain = Chain::new();
        let sentences = self.get_sentences();
        for sentence in sentences {
            chain.feed_str(&sentence);
        }
        chain
    }
}

#[derive(Serialize)]
pub struct ASMChainResponse {
    pub time: f64,
    pub sentence: String,
    pub sentences: u64,
    pub cells: u64
}
