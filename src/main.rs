use crate::core::ASMCore;

mod controllers;
mod core;
mod config;
mod chain;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let core = ASMCore::new()?;
    core.run().await?;
    Ok(())
}
