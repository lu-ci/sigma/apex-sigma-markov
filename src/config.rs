use std::net::{Ipv4Addr, SocketAddrV4};

use serde::Deserialize;
use std::str::FromStr;
use std::path::Path;

#[derive(Clone, Deserialize)]
pub struct ASMConfig {
    pub host: String,
    pub port: u16,
    pub debug: bool,
}

impl Default for ASMConfig {
    fn default() -> Self {
        Self {
            host: "127.0.0.1".to_string(),
            port: 3030,
            debug: false,
        }
    }
}

impl ASMConfig {
    pub fn new() -> anyhow::Result<Self> {
        let path = Path::new("config.yml");
        if path.exists() {
            let content = std::fs::read_to_string(&path)?;
            let cfg = serde_yaml::from_str::<Self>(&content)?;
            let _ = cfg.get_host()?;
            Ok(cfg)
        } else {
            Ok(Self::default())
        }
    }

    pub fn get_host(&self) -> anyhow::Result<SocketAddrV4> {
        Ok(SocketAddrV4::new(
            Ipv4Addr::from_str(&self.host)?,
            self.port,
        ))
    }

    pub fn get_uri(&self) -> String {
        format!("http://{}:{}/", self.host, self.port)
    }
}