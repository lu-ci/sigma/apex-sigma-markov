use std::collections::HashMap;

use markov::Chain;
use warp::{Rejection, Reply};
use warp::http::Response;

use crate::chain::{ASMChain, ASMChainResponse};
use crate::controllers::single::{decompress, exists};

async fn offloader(targets: String, queries: HashMap<String, String>) -> anyhow::Result<Response<String>> {
    let targets = targets.split(",").collect::<Vec<&str>>();
    let target_count = targets.len();
    let mut target_ids = Vec::new();
    let mut all_exist = true;
    for target in targets {
        let target_id = target.parse::<u64>()?;
        let exists = exists(target_id);
        if !exists {
            all_exist = false;
            break;
        } else {
            target_ids.push(target_id)
        }
    }
    let mut sentences = 0;
    let mut cells = 0;
    Ok(if all_exist {
        let start = chrono::Utc::now().timestamp_millis() as f64 / 1_000_f64;
        let mut chain = Chain::new();
        for target_id in target_ids {
            let cont = decompress(target_id)?;
            let item = ASMChain::load(cont)?;
            let stats = item.get_stats();
            sentences += stats[0];
            cells += stats[1];
            for sentence in item.get_sentences() {
                chain.feed_str(&sentence);
            }
        }
        let sentence = if let Some(seed) = queries.get("seed") {
            chain.generate_str_from_token(&seed)
        } else {
            chain.generate_str()
        };
        let finish = chrono::Utc::now().timestamp_millis() as f64 / 1_000_f64;
        let response = ASMChainResponse { time: finish - start, sentence, sentences, cells };
        println!("{} | COMBINED | USERS: {} | EX: {}s", chrono::Utc::now().format("%Y-%m-%d %R"), target_count, finish - start);
        let body = serde_json::to_string(&response)?;
        Response::builder()
            .status(200)
            .header("Content-Type", "application/json")
            .body(body)?
    } else {
        Response::builder()
            .status(404)
            .body("One or more chains were not found.".to_string())?
    }
    )
}

pub async fn combined_handler(targets: String, queries: HashMap<String, String>) -> Result<impl Reply, Rejection> {
    Ok(match offloader(targets, queries).await {
        Ok(reply) => reply,
        Err(err) => Response::builder()
            .status(500)
            .body(format!("Error: {}", err))
            .unwrap(),
    })
}
