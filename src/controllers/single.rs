use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::path::Path;

use warp::{Rejection, Reply};
use warp::http::Response;

use crate::chain::{ASMChain, ASMChainResponse};

const CHAIN_PATH_STR: &str = "chains";

pub fn path(target: u64) -> String {
    let pth = format!("{}/{}.json.gz", CHAIN_PATH_STR, target);
    pth
}

pub fn exists(target: u64) -> bool {
    let pth = path(target);
    let path = Path::new(&pth);
    path.exists()
}

pub fn decompress(target: u64) -> anyhow::Result<String> {
    let pth = path(target);
    let path = Path::new(&pth);
    let mut file = File::open(path)?;
    let mut buffer = Vec::<u8>::new();
    file.read_to_end(&mut buffer)?;
    let mut contents = String::new();
    flate2::read::GzDecoder::new(buffer.as_slice()).read_to_string(&mut contents)?;
    Ok(contents)
}

async fn offloader(target: u64, queries: HashMap<String, String>) -> anyhow::Result<Response<String>> {
    let exists = exists(target);
    Ok(if exists {
        let start = chrono::Utc::now().timestamp_millis() as f64 / 1_000_f64;
        let cont = decompress(target)?;
        let item = ASMChain::load(cont)?;
        let stats = item.get_stats();
        let sentences = stats[0];
        let cells = stats[1];
        let chain = item.get_chain();
        let sentence = if let Some(seed) = queries.get("seed") {
            chain.generate_str_from_token(&seed)
        } else {
            chain.generate_str()
        };
        let finish = chrono::Utc::now().timestamp_millis() as f64 / 1_000_f64;
        println!("{} | SINGLE | UID: {} | EX: {}s", chrono::Utc::now().format("%Y-%m-%d %R"), target, finish - start);
        let response = ASMChainResponse { time: finish - start, sentence, sentences, cells };
        let body = serde_json::to_string(&response)?;
        Response::builder()
            .status(200)
            .header("Content-Type", "application/json")
            .body(body)?
    } else {
        Response::builder()
            .status(404)
            .body("Chain not found.".to_string())?
    })
}

pub async fn single_handler(target: u64, queries: HashMap<String, String>) -> Result<impl Reply, Rejection> {
    Ok(match offloader(target, queries).await {
        Ok(reply) => reply,
        Err(err) => Response::builder()
            .status(500)
            .body(format!("Error: {}", err))
            .unwrap(),
    })
}
